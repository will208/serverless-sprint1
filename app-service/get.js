const AWS = require("aws-sdk");
const { nanoid } = require('nanoid');
const _ = require('underscore');
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {app_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "apps",
      Key: {
        "app_id": app_id
      }
    };
    const result = await docClient.get(tableParams).promise();
    const {Item} = result;
    return utils.send(StatusCodes.OK, {
      message: _.isEmpty(result) ? 'App not found!' : 'App retrieved successfully!',
      data: Item
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to get app details!',
      data: e.message
    });
  }
};