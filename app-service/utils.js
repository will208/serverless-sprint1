'use strict';
const _ = require('underscore');

const send = (statusCode, data) => {
  const responseHeaders = {
    'Content-Type': 'application/json',
    // Required for CORS support to work
    'Access-Control-Allow-Origin': '*',  
    // Required for cookies, authorization headers with HTTPS
    'Access-Control-Allow-Credentials': true
  };
  return {
      statusCode: statusCode,
      headers: responseHeaders,
      body: JSON.stringify(
        data,
        null,
        2
      ),
    };
};

const constructUpdateExpressions = (inputParams) => {
  // Function to generate update expression for DynamoDB automatically based on the inputParams
  let updateExpression = '';
  const finalExpression = {};
  const expAttrNames = {};
  const expAttValues = {};
  const systemValReplacements = {
    status: 'x_status'
  };
  for (let attr in inputParams) {
    let emptyExpAttrNames = false;
    const replacedAttrVal = attr && systemValReplacements[attr] ? systemValReplacements[attr] : '';
    if (replacedAttrVal && !updateExpression) {
      updateExpression += `set #${replacedAttrVal} = :new_${attr}`;
    } else if (replacedAttrVal){
      updateExpression += `, #${replacedAttrVal} = :new_${attr}`;
    }
    if (!replacedAttrVal && attr && !updateExpression) {
      updateExpression += `set ${attr} = :new_${attr}`;
    } else if (!replacedAttrVal && attr){
      updateExpression += `, ${attr} = :new_${attr}`;
    }
    if (attr) {
      expAttValues[`:new_${attr}`] = inputParams[attr];
    }
    if (replacedAttrVal) {
      expAttrNames[`#${replacedAttrVal}`] = attr;
    }
  }
  if (!_.isEmpty(updateExpression)) {
    finalExpression.UpdateExpression = updateExpression;
  }
  if (!_.isEmpty(expAttrNames)) {
    finalExpression.ExpressionAttributeNames = expAttrNames;
  }
  if (!_.isEmpty(expAttValues)) {
    finalExpression.ExpressionAttributeValues = expAttValues;
  }
  return finalExpression;
};

const getQuery = (tableName, queryObj, mappingObj = {}) => {
  const finalExpression = {
    TableName: tableName,
  };
  let keyConditionExpression = '';
  const expAttrNames = {};
  const expAttValues = {};
  for (let attr in queryObj) {
    let emptyExpAttrNames = false;
    if (attr && !keyConditionExpression) {
      keyConditionExpression += `#key_${attr} = :new_${attr}`;
    } else if (attr){
      keyConditionExpression += `, #key_${attr} = :new_${attr}`;
    }
    if (attr) {
      expAttValues[`:new_${attr}`] = queryObj[attr];
    }
    if (attr) {
      expAttrNames[`#key_${attr}`] = mappingObj[attr] ? mappingObj[attr] : attr;
    }
  }
  if (!_.isEmpty(keyConditionExpression)) {
    finalExpression.FilterExpression = keyConditionExpression;
  }
  if (!_.isEmpty(expAttrNames)) {
    finalExpression.ExpressionAttributeNames = expAttrNames;
  }
  if (!_.isEmpty(expAttValues)) {
    finalExpression.ExpressionAttributeValues = expAttValues;
  }
  return finalExpression;
};

module.exports = {
  send,
  constructUpdateExpressions,
  getQuery
};