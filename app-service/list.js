const AWS = require("aws-sdk");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "apps"
    };
    const contact_results = await docClient.scan(tableParams).promise();
    const {Items} = contact_results;
    return utils.send(StatusCodes.OK, {
      message: 'Apps retrieved successfully!',
      data: Items
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to fetch apps!',
      data: e.message
    });
  }

};