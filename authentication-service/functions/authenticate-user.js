const AWS = require('aws-sdk');
const {randomDigits} = require("crypto-secure-random-digit");
const ses = new AWS.SES();
const sns = new AWS.SNS();
const cognito = new AWS.CognitoIdentityServiceProvider();
AWS.config.update({region: process.env.REGION});
const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

module.exports.handler = async(event) => {

    try {

        const { walletName } = JSON.parse(event.body);

        const user = await getUser(walletName);
        
        const localUser = await getUserByWalletNameDynamoDb(walletName);

        if(!localUser)
            return createResponse({ message: "User not found." }, 400);
            
        const email = localUser.email;
        const phone = localUser.phone;
        
        if(!email && !phone)
            return createResponse({ message: "User doesn't have email and phone." }, 400);
            
        let secretLoginCode = randomDigits(6).join('');
        
        if(walletName === "permanentuser.near")
            secretLoginCode = "123456";

        await cognito.adminUpdateUserAttributes({
            UserAttributes: [
                {
                    Name: 'custom:authChallenge',
                    Value: `${secretLoginCode}`
                }
            ],
            UserPoolId: process.env.USER_POOL_ID,
            Username: user.Username
        }).promise();

        if(phone){
            await sendSMS(phone, secretLoginCode);
            return createResponse({ message: "Code sent on phone!", type: "phone" }, 200);
    
        } else if (email){
            await sendEmail(email, secretLoginCode);
            return createResponse({ message: "Code sent on email!", type: "email" }, 200);
        }
        
    } catch (err) {
        console.log(err);
        return createResponse({ message: 'Sorry, we could not find your account.' }, 400);
    }
};

async function getUserByWalletNameDynamoDb(walletName){
    
    const params = {
        FilterExpression: "wallet_id = :walletName ",
        ExpressionAttributeValues: { ":walletName": {S: walletName} },
        TableName: "users"
    }

    const users = await new Promise((resolve, reject) =>
        ddb.scan(params, (err, data) => err ? reject(err) : resolve(data)));

    return users.Count ? 
        AWS.DynamoDB.Converter.unmarshall(users.Items[0]) : null;
}

async function getUser(walletName) {    
    return await cognito.adminGetUser({
        UserPoolId: process.env.USER_POOL_ID,
        Username: walletName
    }).promise();
}

async function sendEmail(emailAddress, secretLoginCode) {
    const email = process.env.FROM_EMAIL;
    const params = {
        Destination: {ToAddresses: [emailAddress]},
        Message: {
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data: `<html><body><p>This is your secret login code:</p>
                           <h3>${secretLoginCode}</h3></body></html>`
                },
                Text: {
                    Charset: 'UTF-8',
                    Data: `Your secret login code: ${secretLoginCode}`
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: 'Your secret login code'
            }
        },
        Source: email
    };
    await ses.sendEmail(params).promise();
}

async function sendSMS(phone, passCode) {
  const params = { Message: "NearAPPs - Your secret code: " + passCode, PhoneNumber: phone };
  await sns.publish(params).promise();
}

function createResponse ( data = { message: "OK" }, statusCode = 200){
    return {
        statusCode,
        body: JSON.stringify(data),
        headers: { 
            "Access-Control-Allow-Origin": "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET,PUT,DELETE,PATCH"
         }
    }
}