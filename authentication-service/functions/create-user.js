const AWS = require('aws-sdk');
require('cross-fetch/polyfill');
const cognito = new AWS.CognitoIdentityServiceProvider();
AWS.config.update({region: process.env.REGION});
const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
const { nanoid } = require('nanoid');
const { connect, keyStores } = require("near-api-js");
const {CognitoUserPool, AuthenticationDetails, CognitoUser} = require('amazon-cognito-identity-js');

const poolData = {
    UserPoolId: process.env.USER_POOL_ID,
    ClientId: process.env.USER_POOL_CLIENT_ID
}

const nearConfig = {
    networkId: 'mainnet',
    nodeUrl: 'https://rpc.mainnet.near.org',
    walletUrl: 'https://wallet.near.org',
    helperUrl: 'https://helper.mainnet.near.org',
    explorerUrl: 'https://explorer.mainnet.near.org',
}

const userPool = new CognitoUserPool(poolData);

module.exports.handler = async(event) =>  {

    try {
        const { email, phone, walletName, fullName} = JSON.parse(event.body)

        if(!email && !phone)
            return createResponse({ message: "Email or phone is needed."}, 400)

        const localUser = await getUserByWalletNameDynamoDb(walletName);
        
        if(localUser)
            return createResponse({ message: "User already exists." }, 400);

        const walletState = await getWalletState(walletName);

        if(walletState !== "AccountDoesNotExist")
            return createResponse( {message: "Wallet already exists on Near."} ,400);

        await createUser(walletName);
        
        await setUserPassword(walletName);
        
        const userDb = await createUserDynamoDb( email, phone, walletName, fullName);

        const authUser = await authenticateUser(walletName);

        return createResponse({
            jwt_access_token: authUser.getAccessToken().getJwtToken(),
            jwt_id_token: authUser.getIdToken().getJwtToken(),
            jwt_refresh_token: authUser.getRefreshToken().getToken(),
            user_info: userDb
        }, 200);

    } catch (err) {
        console.log(err);
        return createResponse({ message: JSON.stringify(err) }, 400);
    }
};

async function authenticateUser(walletName){        

    const authDetails = new AuthenticationDetails({
        Username: walletName,
        Password: "123456",
    });

    const user = new CognitoUser({
        Username: walletName,
        Pool: userPool,
    });

    return await new Promise((resolve, reject) => user.authenticateUser(authDetails,{
        onSuccess: resolve,
        onFailure: reject
    }));
}

async function getWalletState(walletName){
    
    const keyStore = new keyStores.InMemoryKeyStore();

    const near = await connect(Object.assign({ deps: { keyStore:  keyStore} }, nearConfig));

    const account = await near.account(walletName);

    return await account.state().catch(err => err.type);
}

async function getUserByWalletNameDynamoDb(walletName){
    
    const params = {
        FilterExpression: "wallet_id = :walletName ",
        ExpressionAttributeValues: { ":walletName": {S: walletName} },
        TableName: "users"
    }

    const users = await new Promise((resolve, reject) =>
        ddb.scan(params, (err, data) => err ? reject(err) : resolve(data)));

    return users.Count ? 
        AWS.DynamoDB.Converter.unmarshall(users.Items[0]) : null;
}

async function createUser(walletName){

    return cognito.adminCreateUser({
        Username: walletName,
        UserPoolId: poolData.UserPoolId,
        TemporaryPassword: "123456",
        UserAttributes: [],
        DesiredDeliveryMediums: [],
        MessageAction: "SUPPRESS"
    }).promise();
}

async function setUserPassword(walletName){

    return cognito.adminSetUserPassword({
        Username: walletName,
        UserPoolId: poolData.UserPoolId,
        Password: "123456",
        Permanent: true,      
    }).promise();
}

async function createUserDynamoDb( email, phone, walletName, fullName){
    
    const params = {
        Item: {
          "user_id": { "S": nanoid() },
          "wallet_status": { "S": "pending" },
          "status": { "S": "active" },
          "created": { "N": `${new Date().getTime()}` },
          "full_name": { "S": fullName || "" },
          "email": { "S": email || "" },
          "wallet_id": { "S": walletName },
          "phone": { "S": phone || "" },
          "verified": { "BOOL": false }
        },
        TableName: "users"
    };
    
    await new Promise((resolve, reject) =>
        ddb.putItem(params, (err, data) => err ? reject(err) : resolve(data)));
        
    return AWS.DynamoDB.Converter.unmarshall(params.Item);
}

function createResponse ( data = { message: "OK" }, statusCode = 200){
    return {
        statusCode,
        body: JSON.stringify(data),
        headers: { "Access-Control-Allow-Origin": "*" }
    }
}

