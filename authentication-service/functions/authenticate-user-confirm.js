const AWS = require('aws-sdk');
const cognito = new AWS.CognitoIdentityServiceProvider();
require('cross-fetch/polyfill');
const {CognitoUserPool, AuthenticationDetails, CognitoUser} = require('amazon-cognito-identity-js');
AWS.config.update({region: process.env.REGION});
const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

const poolData = {
    UserPoolId: process.env.USER_POOL_ID,
    ClientId: process.env.USER_POOL_CLIENT_ID
}

const userPool = new CognitoUserPool(poolData);

module.exports.handler = async(event) => {

    try {
        const { walletName, nonce } = JSON.parse(event.body)

        const user = await cognito.adminGetUser({
            UserPoolId: process.env.USER_POOL_ID,
            Username: walletName,
        }).promise()

        const authChallenge = user.UserAttributes.filter(function (el) {
            return el.Name === "custom:authChallenge";
        })[0].Value;

        if (authChallenge !== nonce)
            return createResponse({ message: "Authentication Code verification Failed!" }, 403);

        const authUser = await authenticateUser(walletName);

        const localUser = await getUserByWalletNameDynamoDb(walletName);
        
        return createResponse({
            jwt_access_token: authUser.getAccessToken().getJwtToken(),
            jwt_id_token: authUser.getIdToken().getJwtToken(),
            jwt_refresh_token: authUser.getRefreshToken().getToken(),
            user_info: localUser
        }, 200);

    } catch (err) {
        console.log(err);
        return createResponse({
            message: 'Sorry, we could not verify authentication code.'
        }, 400);
    }
};

async function getUserByWalletNameDynamoDb(walletName){
    
    const params = {
        FilterExpression: "wallet_id = :walletName ",
        ExpressionAttributeValues: { ":walletName": {S: walletName} },
        TableName: "users"
    }

    const users = await new Promise((resolve, reject) =>
        ddb.scan(params, (err, data) => err ? reject(err) : resolve(data)));

    return users.Count ? 
        AWS.DynamoDB.Converter.unmarshall(users.Items[0]) : null;
}

async function authenticateUser(walletName){        

    const authDetails = new AuthenticationDetails({
        Username: walletName,
        Password: "123456",
    });

    const user = new CognitoUser({
        Username: walletName,
        Pool: userPool,
    });

    return await new Promise((resolve, reject) => user.authenticateUser(authDetails,{
        onSuccess: resolve,
        onFailure: reject
    }));
}

function createResponse ( data = { message: "OK" }, statusCode = 200){
    return {
        statusCode,
        body: JSON.stringify(data),
        headers: { "Access-Control-Allow-Origin": "*" }
    }
}