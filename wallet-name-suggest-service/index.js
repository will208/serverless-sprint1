const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});
const { connect, keyStores } = require("near-api-js");

const nearConfig = {
    networkId: 'mainnet',
    nodeUrl: 'https://rpc.mainnet.near.org',
    walletUrl: 'https://wallet.near.org',
    helperUrl: 'https://helper.mainnet.near.org',
    explorerUrl: 'https://explorer.mainnet.near.org',
}

const dictionary = require('./includes/dictionary');	


exports.handler = async(event, context) => {

	const walletName = (event.queryStringParameters || event.pathParameters).walletName || "";
	let suggestionCount = event.queryStringParameters ? event.queryStringParameters.suggestionCount : 1;
	suggestionCount = suggestionCount > 100 ? 100 : suggestionCount;
	let returnValue = [];

	if (isEmpty(await getUserByWalletNameDynamoDb(walletName)) 
		&& !(await checkWalletNameExistsOnBlockchain(walletName))) {
		return createResponse({ message: 'Wallet name available.'} , 200);
	} else {
		for (let i = 0; i < suggestionCount; i++) {
			const newWalletName = dictionary.getPrimary() + dictionary.getSecondary();

			// Checks on the walletName, to make sure it is unique and not > 12 characters
			if (newWalletName.length >= 12) {
				i--;
			} else {
				// Suggest alternative
				if (isEmpty(await getUserByWalletNameDynamoDb(newWalletName)) 
					&& !(await checkWalletNameExistsOnBlockchain(walletName))) {
					returnValue.push(newWalletName);
				} else {
					i--;
				}
			}
		}

		let nextFound = false;
		let nextCount = 0;
		
		while (nextFound == false) {
			nextCount++;
			const nextwalletNameCount = walletName + nextCount;
			if (isEmpty(await getUserByWalletNameDynamoDb(nextwalletNameCount)) 
				&& !(await checkWalletNameExistsOnBlockchain(walletName))) {
				returnValue.push(nextwalletNameCount);
				nextFound = true;
			}
		}

		return createResponse({ data: returnValue} , 200)
	}
}

async function checkWalletNameExistsOnBlockchain(walletName){
	const walletState = await getWalletState(walletName);
	return walletState !== "AccountDoesNotExist";
}

async function getWalletState(walletName){
    const keyStore = new keyStores.InMemoryKeyStore();
    const near = await connect(Object.assign({ deps: { keyStore:  keyStore} }, nearConfig));
    const account = await near.account(walletName);
    return await account.state().catch(err => err.type);
}

async function getUserByWalletNameDynamoDb(walletName){
    
    const params = {
        FilterExpression: "wallet_id = :walletName ",
        ExpressionAttributeValues: { ":walletName": {S: walletName} },
        TableName: "users"
    }

    const users = await new Promise((resolve, reject) =>
        ddb.scan(params, (err, data) => err ? reject(err) : resolve(data)));

    return users.Count ? 
        AWS.DynamoDB.Converter.unmarshall(users.Items[0]) : null;
}

function isEmpty(obj) {
    return !obj || Object.keys(obj).length === 0;
}

function createResponse ( data = { message: "OK" }, statusCode = 200){
    return {
        statusCode,
        body: JSON.stringify(data),
        headers: { "Access-Control-Allow-Origin": "*" }
    }
}