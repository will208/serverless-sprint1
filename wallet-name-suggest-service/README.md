# napps-api-utils
Storing utilities that are used on the backend of our NearApps Gateway

# Lambda function setup
As much as possible, please hook into existing functions.  They have been setup to handle normal flow, along with errors.  Any include related code that is only related to a single or few functions, please push into the include folder.

For any global related variables (like database connection strings), please post them into the parameter store and code them into the _getParameterStoreVariablesPopulate function. 