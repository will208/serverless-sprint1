const AWS = require("aws-sdk");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

utils.setAWSRegion();

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {user_id} = pathParameters;
  let { body: params } = event;
  const body = JSON.parse(params);
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
        TableName: "users",
        Key: {
            "user_id": user_id
        },
        ...utils.constructUpdateExpressions(body)
    };
    // console.log('tableParams ---> ', JSON.stringify(tableParams));
    const result = await docClient.update(tableParams).promise();
    // const {Item} = contact_result;
    return utils.send(StatusCodes.OK, {
      message: 'User updated successfully!',
      data: result
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to update user',
      data: e.message
    });
  }
};