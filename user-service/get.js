const AWS = require("aws-sdk");
const { nanoid } = require('nanoid');
const _ = require('underscore');
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

utils.setAWSRegion();

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {user_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "users",
      Key: {
        "user_id": user_id
      }
    };
    const result = await docClient.get(tableParams).promise();
    const {Item} = result;
    return utils.send(StatusCodes.OK, {
      message: _.isEmpty(result) ? 'User not found!' : 'User retrieved successfully!',
      data: Item
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to get user details!',
      data: e.message
    });
  }
};