const AWS = require("aws-sdk");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

utils.setAWSRegion();

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {user_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "users",
      Key: {
          "user_id": user_id
      },
      UpdateExpression: 'set #user_status = :new_status, updated = :updt_date',
      ExpressionAttributeNames: {
        "#user_status": "status"
      },
      ExpressionAttributeValues: {
       ':new_status': "deleted",
       ':updt_date': +new Date
      }
  };
    const result = await docClient.update(tableParams).promise();
    // const {Item} = contact_result;
    return utils.send(StatusCodes.OK, {
      message: 'User deleted successfully!',
      data: result
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to delete user',
      data: e.message
    });
  }

};