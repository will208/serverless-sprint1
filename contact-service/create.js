const AWS = require("aws-sdk");
const { nanoid } = require('nanoid');
const { StatusCodes } = require('http-status-codes');
const utils = require("./utils");

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const { body } = event;
  const params = JSON.parse(body);

  if (!params.owner_id) {
    return utils.send(StatusCodes.BAD_REQUEST, {
      message: 'Logged in user id (owner_id) missing in the request!'
    });
  }

  // Check if the ids are already present in DB (TODO) and add contacts
  const docClient = new AWS.DynamoDB.DocumentClient();
  const contact_id = nanoid();
  try {
    // Create contacts
    const tableParams = {
        TableName: "contacts",
        Item: {
            "contact_id": contact_id,
            ...params,
            "status": "active",
            "archived_date": "",
            "created": +new Date,
            "updated": +new Date
        }
    };
    // console.log('tableParams - ', JSON.stringify(tableParams));
    const add_results = await docClient.put(tableParams).promise();
    return utils.send(StatusCodes.OK, {
      message: 'Contact added successfully!',
      data: {
        contact_id
      },
      debug: add_results
    });
  } catch (e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Error adding contacts to the user!',
      data: e.message
    });
  }
};