const AWS = require("aws-sdk");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {user_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "contacts",
      FilterExpression: "owner_id = :ownerID",
      ExpressionAttributeValues: {
        ":ownerID": user_id
      }
    };
    const contact_results = await docClient.scan(tableParams).promise();
    const {Items} = contact_results;
    return utils.send(StatusCodes.OK, {
      message: 'Contacts retrieved successfully!',
      data: Items
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to fetch contacts list!',
      data: e.message
    });
  }

};