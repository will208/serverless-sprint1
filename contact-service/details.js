const AWS = require("aws-sdk");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {contact_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "contacts",
      Key: {
        "contact_id": contact_id
      }
    };
    const contact_result = await docClient.get(tableParams).promise();
    const {Item} = contact_result;
    return utils.send(StatusCodes.OK, {
      message: 'Contact retrieved successfully!',
      data: Item
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to fetch contacts list!',
      data: e.message
    });
  }

};