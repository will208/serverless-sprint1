const AWS = require("aws-sdk");
const utils = require("./utils");
const { ReasonPhrases, StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {contact_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "contacts",
      Key: {
          "contact_id": contact_id
      },
      UpdateExpression: 'set #contact_status = :new_status, archived_date = :arch_date',
      ExpressionAttributeNames: {
        "#contact_status": "status"
      },
      ExpressionAttributeValues: {
       ':new_status': "archived",
       ':arch_date': +new Date
      }
  };
    const result = await docClient.update(tableParams).promise();
    // const {Item} = contact_result;
    return utils.send(StatusCodes.OK, {
      message: 'Contact deleted successfully!',
      data: result
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to delete contact',
      data: e.message
    });
  }

};