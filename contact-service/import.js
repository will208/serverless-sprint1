const AWS = require("aws-sdk");
const { nanoid } = require('nanoid');
const { StatusCodes } = require('http-status-codes');
const utils = require("./utils");

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  let { body: params } = event;
  const body = JSON.parse(params);

  // Check if the ids are already present in DB (TODO) and add contacts
  const docClient = new AWS.DynamoDB.DocumentClient();
  let owner_exist = true;
  const contacts = [];
  // Inject contact IDs
  for (let i=0; i<body.length; i++) {
    const contactid = nanoid();
    contacts.push({
      PutRequest: {
        Item: {
            "contact_id": contactid,
            ...body[i],
            "status": "active",
            "created": +new Date,
            "modified": +new Date,
        }
      }
    });
    if (!body[i].owner_id) {
      owner_exist = false;
      break;
    }
  }
  if (!owner_exist) {
    return utils.send(StatusCodes.BAD_REQUEST, {
      message: 'Logged in user id (owner_id) missing in one or more contacts!'
    });
  }
  let tableParams = {
      RequestItems: {
          contacts
      }
  };
  try {
    // Create contacts
    // console.log(JSON.stringify(tableParams));
    const combined_add_results = await docClient.batchWrite(tableParams).promise();
    return utils.send(StatusCodes.OK, {
      message: 'Contacts added successfully!',
      data: combined_add_results
    });
  } catch (e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Error adding contacts to the user!',
      data: e.message
    });
  }
};