const AWS = require("aws-sdk");
const _ = require("underscore");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {nft_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "nfts",
      Key: {
        "nft_id": nft_id
      }
    };
    const nft_result = await docClient.get(tableParams).promise();
    const {Item} = nft_result;
    return utils.send(StatusCodes.OK, {
      message: _.isEmpty(nft_result) ? 'NFT not found!' : 'NFT retrieved successfully!',
      data: Item
    });
  } catch (e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: e.message
    });
  }
};