'use strict';
const _ = require('underscore');
const https = require('https');
const AWS = require("aws-sdk");
const s3 = new AWS.S3();
// S3 details for the NFT files
const bucket = "near-lambda-nft-files";
const PNG_MIME_TYPE = "image/png";
const JPEG_MIME_TYPE = "image/jpeg";
const JPG_MIME_TYPE = "image/jpg";
const WEBP_MIME_TYPE = "image/webp";
const V_MP4_MIME_TYPE = "video/mp4";
const A_MP4_MIME_TYPE = "audio/mp4";
const MP3_MIME_TYPE = "audio/mpeg";
const MIME_TYPES = [PNG_MIME_TYPE, JPEG_MIME_TYPE, JPG_MIME_TYPE, WEBP_MIME_TYPE, V_MP4_MIME_TYPE, A_MP4_MIME_TYPE, MP3_MIME_TYPE];

const send = (statusCode, data) => {
  const responseHeaders = {
    'Content-Type': 'application/json',
    // Required for CORS support to work
    'Access-Control-Allow-Origin': '*',  
    // Required for cookies, authorization headers with HTTPS
    'Access-Control-Allow-Credentials': true
  };
  return {
      statusCode: statusCode,
      headers: responseHeaders,
      body: JSON.stringify(
        data,
        null,
        2
      ),
    };
};

const bytesToMegaBytes = bytes => bytes / (1024*1024);

const constructUpdateExpressions = (inputParams) => {
  // Function to generate update expression for DynamoDB automatically based on the inputParams
  let updateExpression = '';
  const finalExpression = {};
  const expAttrNames = {};
  const expAttValues = {};
  const systemValReplacements = {
    status: 'x_status'
  };
  for (let attr in inputParams) {
    let emptyExpAttrNames = false;
    const replacedAttrVal = attr && systemValReplacements[attr] ? systemValReplacements[attr] : '';
    if (replacedAttrVal && !updateExpression) {
      updateExpression += `set #${replacedAttrVal} = :new_${attr}`;
    } else if (replacedAttrVal){
      updateExpression += `, #${replacedAttrVal} = :new_${attr}`;
    }
    if (!replacedAttrVal && attr && !updateExpression) {
      updateExpression += `set ${attr} = :new_${attr}`;
    } else if (!replacedAttrVal && attr){
      updateExpression += `, ${attr} = :new_${attr}`;
    }
    if (attr) {
      expAttValues[`:new_${attr}`] = inputParams[attr];
    }
    if (replacedAttrVal) {
      expAttrNames[`#${replacedAttrVal}`] = attr;
    }
  }
  if (!_.isEmpty(updateExpression)) {
    finalExpression.UpdateExpression = updateExpression;
  }
  if (!_.isEmpty(expAttrNames)) {
    finalExpression.ExpressionAttributeNames = expAttrNames;
  }
  if (!_.isEmpty(expAttValues)) {
    finalExpression.ExpressionAttributeValues = expAttValues;
  }
  return finalExpression;
};

const uploadToS3 = (key, buffer, mimeType) =>
  new Promise((resolve, reject) => {
    s3.upload(
      { Bucket: bucket, Key: key, Body: buffer, ContentType: mimeType, ACL: 'public-read' },
      function(err, data) {
          if (err) reject(err);
          resolve(data);
      })
  })

const getSignedUrl = (fileObj) => {
  return s3.getSignedUrl("getObject", fileObj);
};

const getQuery = (tableName, queryObj, mappingObj = {}) => {
  const finalExpression = {
    TableName: tableName,
  };
  let keyConditionExpression = '';
  const expAttrNames = {};
  const expAttValues = {};
  for (let attr in queryObj) {
    let emptyExpAttrNames = false;
    if (attr && !keyConditionExpression) {
      keyConditionExpression += `#key_${attr} = :new_${attr}`;
    } else if (attr){
      keyConditionExpression += `, #key_${attr} = :new_${attr}`;
    }
    if (attr) {
      expAttValues[`:new_${attr}`] = queryObj[attr];
    }
    if (attr) {
      expAttrNames[`#key_${attr}`] = mappingObj[attr] ? mappingObj[attr] : attr;
    }
  }
  if (!_.isEmpty(keyConditionExpression)) {
    finalExpression.FilterExpression = keyConditionExpression;
  }
  if (!_.isEmpty(expAttrNames)) {
    finalExpression.ExpressionAttributeNames = expAttrNames;
  }
  if (!_.isEmpty(expAttValues)) {
    finalExpression.ExpressionAttributeValues = expAttValues;
  }
  return finalExpression;
};

const getTransactionsForNFT = (nft_id, user_id) => {
  // console.log('getTransactionsForNFT >>> ', nft_id, user_id);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const tableParams = {
    TableName: "transactions",
    FilterExpression: "recipient_id = :ownerID AND transaction_item_id = :nftID",
    ExpressionAttributeValues: {
      ":ownerID": user_id,
      ":nftID": nft_id
    }
  };
  console.log('tableParams -> ', JSON.stringify(tableParams));
  return docClient.scan(tableParams).promise();
};

const createNFT = (id, params) => {
  const docClient = new AWS.DynamoDB.DocumentClient();
  const tableParams = {
    TableName: "nfts",
    Item: {
        "nft_id": id,
        ...params,
        "status": "active",
        "created": +new Date,
        "updated": +new Date
    }
  };
  return docClient.put(tableParams).promise();
};

const createTransaction = (params) => {
  // console.log('createTransaction - ', params);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const tableParams = {
    TableName: "transactions",
    Item: {
        "transaction_id": params.transaction_id,
        "sender_id": params.owner_id,
        "transaction_item_id": params.nftid,
        "transaction_value": params.transaction_value,
        "type": params.type,
        "created": +new Date,
        "updated": +new Date,
        "blockchain_status": "pending",
        "status": params.status
    }
  };
  if (params.recipient_id) {
    tableParams.Item.recipient_id = params.recipient_id;
  }
  return docClient.put(tableParams).promise();
};

const triggerAdCall = async (trackerObj) => {
  // https://www.tp88trk.com/3J67C/2QNG6FC/?source_id=TAPJOY_GENERIC_SOURCE&sub1=
  const { value } = trackerObj;
  const options = {
    host: 'tp88trk.com',
    path: `/3J67C/2QNG6FC/?source_id=TAPJOY_GENERIC_SOURCE&sub1=${value}`,
    method: 'GET'
  };
  if (!value) {
    console.log('triggerAdCall ERROR: No tracker value found!', trackerObj);  
  }
  console.log('triggerAdCall CALLING: ', options);
  https.request(options, (response) => {
    console.log(`triggerAdCall STATUS: ${response.statusCode}`);
  }).end();
};

const getNFT = (nft_id) => {
  const docClient = new AWS.DynamoDB.DocumentClient();
  const tableParams = {
    TableName: "nfts",
    Key: {
      "nft_id": nft_id
    }
  };
  return docClient.get(tableParams).promise();
}

const updateNFT = (nft_id, params) => {
  const docClient = new AWS.DynamoDB.DocumentClient();
  const tableParams = {
      TableName: "nfts",
      Key: {
          "nft_id": nft_id
      },
      ...constructUpdateExpressions(params)
  };
  console.log('tableParams ---> ', JSON.stringify(tableParams));
  return docClient.update(tableParams).promise();
};

module.exports = {
  send,
  bytesToMegaBytes,
  constructUpdateExpressions,
  uploadToS3,
  getSignedUrl,
  getQuery,
  getTransactionsForNFT,
  createNFT,
  createTransaction,
  triggerAdCall,
  getNFT,
  updateNFT
};