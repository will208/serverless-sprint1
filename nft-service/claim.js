const AWS = require("aws-sdk");
const _ = require("underscore");
const { nanoid } = require('nanoid');
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {nft_id} = pathParameters;
  let { body: params } = event;
  const body = JSON.parse(params);
  const docClient = new AWS.DynamoDB.DocumentClient();
  // Requires owner_id (logged in user), NFT ID, Transaction ID
  // Prerequisite - pending request should exist 
  const {owner_id: user_id, type} = body;
  if (!nft_id) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'NFT ID is missing from the NFC claim request!'
    });
  }
  if (!user_id) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'User ID is missing from the NFC claim request!'
    });
  }
  try {
      const nftResults = await utils.getNFT(nft_id);
      const {Item: nftResult} = nftResults ? nftResults : {};
      if (!nftResult || _.isEmpty(nftResult)) {
        return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
          message: `NFT doesn't exist to proceed with the claim!`
        });
      }
      const tResults = await utils.getTransactionsForNFT(nft_id, user_id);
      const { Items: transactionsForNFT } = tResults;
      if (!transactionsForNFT || !transactionsForNFT.length) {
        return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
          message: 'Unable to claim this NFT. Try again later!'
        });
      }
      // No completed should exist and pending should exist
      const completedTransactionResults = transactionsForNFT.filter(t => t.status === 'completed');
      const pendingTransactionResults = transactionsForNFT.filter(t => t.status === 'pending');
      if (!completedTransactionResults.length && pendingTransactionResults.length) {
        const [unclaimedTransaction] = pendingTransactionResults;
        // Claim the transaction
        const claimResult = await utils.createTransaction({
          ...unclaimedTransaction,
          transaction_id: nanoid(),
          status: 'completed',
          nftid: unclaimedTransaction.transaction_item_id,
          owner_id: unclaimedTransaction.sender_id,
          type: type ? type :'gift'
        });
        // Update the NFT status and owner id
        const updatedNFTParams = { 
          ...nftResult, 
          status: 'active',
          owner_id: user_id,
          updated: +new Date
        };
        delete updatedNFTParams.nft_id;
        const updateResult = await utils.updateNFT(nft_id, updatedNFTParams);
        return utils.send(StatusCodes.OK, {
          message: 'NFT claimed successfully!',
          debug: {
            claimResult,
            updateResult
          }
        });
      } else {
        return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
          message: 'Unable to claim this NFT. Try again later!'
        });
      }
    // }
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to claim NFT',
      data: e.message
    });
  }
};