const AWS = require("aws-sdk");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');
const formParser = require("./formParser");
const MAX_SIZE = 100000000; // 100MB

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const { pathParameters } = event;
  const { nft_id } = pathParameters;
  const docClient = new AWS.DynamoDB.DocumentClient();
  try {
    const formData = await formParser.parser(event, MAX_SIZE);
    const {files, data} = formData;
    const params = JSON.parse(data);

    const getNFTParams = {
      TableName: "nfts",
      Key: {
        "nft_id": nft_id
      }
    };
    const nft_result = await docClient.get(getNFTParams).promise();
    const {Item: nft_db_row} = nft_result;
    if (!nft_db_row || !nft_db_row.nft_id) {
      return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
        message: 'No NFT exists with the ID to update!',
        data: nft_db_row
      });
    }

    if (files && files.length) {
      const file = files[0];
      const {filename: actFile} = file;
      const originalKey = `${nft_id}_original_${actFile.filename}`;

      // File upload
      const [originalFile] = await Promise.all([
          utils.uploadToS3(originalKey, file.content, file.contentType),
      ]);

      const signedOriginalUrl = utils.getSignedUrl({ Bucket: originalFile.Bucket, Key: originalKey, Expires: 63113904 });
      params.file_url = signedOriginalUrl;
    }

    const tableParams = {
        TableName: "nfts",
        Key: {
            "nft_id": nft_id
        },
        ...utils.constructUpdateExpressions(params)
    };
    // console.log('tableParams ---> ', JSON.stringify(tableParams));
    const result = await docClient.update(tableParams).promise();
    // const {Item} = contact_result;
    return utils.send(StatusCodes.OK, {
      message: 'NFT updated successfully!',
      data: result
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to update NFT',
      data: e.message
    });
  }
};