const AWS = require("aws-sdk");
const utils = require("./utils");
const { ReasonPhrases, StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {nft_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "nfts",
      Key: {
          "nft_id": nft_id
      },
      UpdateExpression: 'set #nft_status = :new_status, updated = :upd_date',
      ExpressionAttributeNames: {
        "#nft_status": "status"
      },
      ExpressionAttributeValues: {
       ':new_status': "deleted",
       ':upd_date': +new Date
      }
  };
    const result = await docClient.update(tableParams).promise();
    // const {Item} = contact_result;
    return utils.send(StatusCodes.OK, {
      message: 'NFT deleted successfully!',
      data: result
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to remove NFT!',
      data: e.message
    });
  }

};