const AWS = require("aws-sdk");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const docClient = new AWS.DynamoDB.DocumentClient();
  const { queryStringParameters } = event;
  const queryMapping = {
    user_id: 'owner_id'
  };

  // console.log('event - ', JSON.stringify(queryStringParameters));
  try {
    let result = null;
    let tableParams = {};
    if (queryStringParameters) {
      tableParams = utils.getQuery("nfts", queryStringParameters, queryMapping);
    } else {
      tableParams = {
        TableName: "nfts"
      };
    }
    result = await docClient.scan(tableParams).promise();
    return utils.send(StatusCodes.OK, {
      message: 'NFTs fetched successfully!',
      data: result && result.Items ? result.Items : []
    });
  } catch (e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: e.message
    });
  }
};