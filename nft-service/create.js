'use strict';
const AWS = require("aws-sdk");
const { nanoid } = require('nanoid');
const s3 = new AWS.S3();
const formParser = require("./formParser");
const { StatusCodes } = require('http-status-codes');
const utils = require("./utils");
const MAX_SIZE = 100000000; // 100MB

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  // Upload file in the S3 bucket - get file id
  // Post NFT data with file ID to dynamo
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const formData = await formParser.parser(event, MAX_SIZE);
    const {files, data} = formData;
    const file = files[0];
    const nftid = nanoid();
    const {filename: actFile} = file;
    const originalKey = `${nftid}_original_${actFile.filename}`;
    const params = JSON.parse(data);

    // File, Title, Description, Properties (key value pairs)
    // Return ID

    if (!params) {
      return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
        message: 'NFT creation requires File, Title, Description, Properties (key value pairs) in the request'
      });
    }

    if (!params.title) {
      return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
        message: 'Title is missing from the NFC creation request!'
      });
    }

    if (!params.description) {
      return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
        message: 'Description is missing from the NFC creation request!'
      });
    }

    if (!params.owner_id) {
      return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
        message: 'Owner ID (owner_id) is missing from the NFC creation request!'
      });
    }

    if (actFile.size && utils.bytesToMegaBytes(actFile.size) > 100) {
      return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
        message: 'File size is greater than 100MB from the NFC creation request!'
      });
    }

    const [originalFile] = await Promise.all([
        utils.uploadToS3(originalKey, file.content, file.contentType),
    ]);

    // console.log('originalFile ---> ', JSON.stringify(originalFile));
    if (originalFile && originalFile.Location) {
      params.file_url = originalFile.Location;
    } else {
      const signedOriginalUrl = utils.getSignedUrl({ Bucket: originalFile.Bucket, Key: originalKey, Expires: 63113904 });
      params.file_url = signedOriginalUrl;
    }

    // Trigger the ad tracking id
    if (params.tracker) {
      utils.triggerAdCall(params.tracker);
    }
    // Store in Dynamo DB
    const result = await utils.createNFT(nftid, params);
    let transaction_result = null;
    // action_type is mine -> NFT must be sent to mine
    if (params.action_type && params.action_type === 'mine') {
      // Todo: Invoke transaction-service lambda for mining
      const transaction_id = nanoid();      
      transaction_result = await utils.createTransaction({
        transaction_id, 
        nftid,
        recipient_id: '',
        type: 'regular',
        status: 'mined',
        ...params
      });
    }
    const addedNFT = await utils.getNFT(nftid);
    const {Item} = addedNFT ? addedNFT : {};

    // Retrieve NFT data to send it back
    return utils.send(StatusCodes.OK, {
      message: 'NFT creation successful!',
      data: Item ? {...Item} : {},
      debug: {
        result,
        transaction_result
      }
    });
  } catch (e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: e.message
    });
  }
};