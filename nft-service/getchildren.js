const AWS = require("aws-sdk");
const _ = require("underscore");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {nft_id} = pathParameters;

  try {
    const docClient = new AWS.DynamoDB.DocumentClient();
    const tableParams = {
      TableName: "nfts",
      FilterExpression: "parent_id = :parentID",
      ExpressionAttributeValues: {
        ":parentID": nft_id
      }
    };
    console.log('tableParams -> ', JSON.stringify(tableParams));
    const nft_result = await docClient.scan(tableParams).promise();

    return utils.send(StatusCodes.OK, {
      message: `Children for NFT - ${nft_id} retrieved successfully!`,
      data: nft_result && nft_result.Items ? nft_result.Items : []
    });
  } catch (e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: e.message
    });
  }
};