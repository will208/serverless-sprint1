const AWS = require("aws-sdk");
const { nanoid } = require('nanoid');
const { StatusCodes } = require('http-status-codes');
const utils = require("./utils");

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  // tid, from, to, nft_id, created, updated, status
  const { body } = event;
  const params = JSON.parse(body);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const transaction_id = nanoid();
  const nftRequests = [];
  const transactionRequests = [];

  if (!params.recipient_id) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Recipient ID (recipient_id) is missing from the request!',
      data: params
    });
  }
  if (!params.sender_id) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Sender ID (sender_id) is missing from the request!',
      data: params
    });
  }
  if (!params.transaction_item_id) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'NFT ID (transaction_item_id) is missing from the request!',
      data: params
    });
  }
  // utils.getAppConfig();
  try {
    let result = null;
    let nftResults = null;
    let transactionResults = null;
    if (params.recipient_id && typeof params.recipient_id === 'string') {
      let shouldTriggerNotification = false;
      params.transaction_id = transaction_id;
      if (params.type === 'regular') {
        params.status = "completed"; // 
      } else {
        shouldTriggerNotification = true;
      }
      result = await utils.createTransaction(params);
      if (shouldTriggerNotification) {
        const nft_transaction_recipient_pairs = [{
          nft_id: params.transaction_item_id,
          transaction_id,
          recipient_id: params.recipient_id
        }];
        await utils.triggerNotificationsForUsers({
          sender_id: params.sender_id, 
          ntr_pairs: nft_transaction_recipient_pairs
        }, [params.recipient_id]);
      }
    }
    if (params.recipient_id && Array.isArray(params.recipient_id) && params.recipient_id.length === 1) {
      let shouldTriggerNotification = false;
      const [recipient_id] = params.recipient_id;
      params.recipient_id = recipient_id;
      params.transaction_id = transaction_id;
      if (params.type === 'regular') {
        params.status = "completed"; // 
      } else {
        shouldTriggerNotification = true;
      }
      result = await utils.createTransaction(params);
      if (shouldTriggerNotification) {
        const nft_transaction_recipient_pairs = [{
          nft_id: params.transaction_item_id,
          transaction_id,
          recipient_id: params.recipient_id
        }];
        await utils.triggerNotificationsForUsers({
          sender_id: params.sender_id, 
          ntr_pairs: nft_transaction_recipient_pairs
        }, [params.recipient_id]);
      }
    }
    // Multiple recipients
    if (params.recipient_id && Array.isArray(params.recipient_id) && params.recipient_id.length && params.transaction_item_id) {
      // Clone the passed NFT
      const nft_result = await utils.getNFT(params.transaction_item_id);
      const nft_transaction_recipient_pairs = [];
      let {Item: nftObj} = nft_result;
      delete nftObj.nft_id;
      delete nftObj.created;
      delete nftObj.updated;

      for (let i=0; i < params.recipient_id.length; i++) {
        // Create NFT, Add pending Transaction
        const nft_id = nanoid();
        // Get NFT (n-1) and create copies
        // if (i > 0) {
        nftRequests.push(utils.createNFT({nft_id, parent_id: params.transaction_item_id, ...nftObj}));
        // }
        const inputParams = {...params};
        const new_tid = nanoid();
        inputParams.transaction_id = new_tid;
        inputParams.recipient_id = params.recipient_id[i];
        inputParams.transaction_item_id = nft_id;
        inputParams.type = "unclaimed";
        inputParams.status = "pending";
        inputParams.is_bulk = true;
        transactionRequests.push(utils.createTransaction(inputParams));
        // store the nft-transaction pairs
        nft_transaction_recipient_pairs.push({ nft_id, transaction_id: new_tid, recipient_id: params.recipient_id[i] });
      }
      console.log('nft_transaction_recipient_pairs - ', nft_transaction_recipient_pairs);
      nftResults = await Promise.all(nftRequests);
      transactionResults = await Promise.all(transactionRequests);
      // Send out notifications to the recipients
      await utils.triggerNotificationsForUsers({
        sender_id: params.sender_id, 
        ntr_pairs: nft_transaction_recipient_pairs
      }, params.recipient_id);
    }
    return utils.send(StatusCodes.OK, {
      message: 'Transaction created successfully!',
      data: result,
      nftResults,
      transactionResults
    });
  } catch (err) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Error occured while creating a transaction!',
      data: err.message
    });
  }
};