const AWS = require("aws-sdk");
const utils = require("./utils");
const { ReasonPhrases, StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {transaction_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "transactions",
      Key: {
          "transaction_id": transaction_id
      },
      UpdateExpression: 'set #transaction_status = :new_status, updated = :updt_date',
      ExpressionAttributeNames: {
        "#transaction_status": "status"
      },
      ExpressionAttributeValues: {
       ':new_status': "cancelled",
       ':updt_date': +new Date
      }
  };
    const result = await docClient.update(tableParams).promise();
    // const {Item} = contact_result;
    return utils.send(StatusCodes.OK, {
      message: 'Transaction deleted successfully!',
      data: result
    });
  } catch(e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: 'Unable to delete transaction',
      data: e.message
    });
  }

};