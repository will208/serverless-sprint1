const AWS = require("aws-sdk");
const utils = require("./utils");
const moment = require('moment');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  // tid, from, to, nft_id, created, updated, status
  const {pathParameters} = event;
  const {nft_id} = pathParameters;
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "transactions",
      // ProjectionExpression: "relationid",
      FilterExpression: "transaction_item_id = :nft_id",
      ExpressionAttributeValues: {
        ":nft_id": nft_id
      }
    };
    const transaction_results = await docClient.scan(tableParams).promise();
    console.log('transaction_results -> ', transaction_results);
    const transactions = transaction_results && transaction_results.Items ? transaction_results.Items : [];
    if (transactions && transactions.length) {
      const party_ids = [];
      transactions.map(transaction => {
        if (transaction.recipient_id && party_ids.indexOf(transaction.recipient_id) === -1) {
          party_ids.push(transaction.recipient_id);
        }
        if (transaction.sender_id && party_ids.indexOf(transaction.sender_id) === -1) {
          party_ids.push(transaction.sender_id);
        }
      });
      const formattedIDs = party_ids.map(party_id => {
        return {
          user_id: party_id
        };
      });
      const queryParams = {
        RequestItems: {
          users: {
            Keys: formattedIDs
          }
        }
      };
      const results = await docClient.batchGet(queryParams).promise();
      const transaction_parties = (results && results.Responses && results.Responses.users) ? results.Responses.users : [];
      // Create counterparty object
      const parties_map = {};
      transaction_parties.map(party => {
        const {wallet_id, full_name, email, phone, user_id} = party;
        parties_map[user_id] = { wallet_id, full_name, email, phone };
      });
      // console.log('parties_map - ', parties_map);
      transactions.map(transaction => {
        const {sender_id, recipient_id} = transaction;
        // console.log(sender_id, recipient_id);
        if (sender_id && parties_map[sender_id]) {
          transaction.sender = parties_map[sender_id];
        }
        if (recipient_id && parties_map[recipient_id]) {
          transaction.recipient = parties_map[recipient_id];
        }
        transaction.formattedtime = moment(transaction.updated).fromNow();
      });
    }
    return utils.send(200, {
      message: `Transactions for NFT ${nft_id} retrieved successfully!`,
      data: transactions
    });
  } catch (e) {
    return utils.send(500, {
      message: e.message
    });
  }
};