const AWS = require("aws-sdk");
const utils = require("./utils");
const moment = require('moment');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  // tid, from, to, nft_id, created, updated, status
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "sample",
      ScanIndexForward: true
      // ProjectionExpression: "relationid",
      // FilterExpression: "sender_id = :fromID OR recipient_id = :toID",
      // ExpressionAttributeValues: {
      //   ":fromID": user_id,
      //   ":toID": user_id
      // }
    };
    console.log(JSON.stringify(tableParams));
    const transaction_results = await docClient.scan(tableParams).promise();
    console.log(JSON.stringify(transaction_results));
    const transactions = transaction_results && transaction_results.Items ? transaction_results.Items : [];
    transactions.map(transaction => {
      transaction.updatedformatted = moment(transaction.updated).fromNow();
      transaction.createdformatted = moment(transaction.created).fromNow();
    });
    return utils.send(200, {
      message: 'Transactions retrieved successfully!',
      data: transactions
    });
  } catch (e) {
    return utils.send(500, {
      message: e.message
    });
  }
};