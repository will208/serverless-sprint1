const AWS = require("aws-sdk");
const _ = require("underscore");
const utils = require("./utils");
const { StatusCodes } = require('http-status-codes');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  const {pathParameters} = event;
  const {transaction_id} = pathParameters;
  
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "transactions",
      Key: {
        "transaction_id": transaction_id
      }
    };
    console.log(JSON.stringify(tableParams));
    const transaction_result = await docClient.get(tableParams).promise();
    const {Item} = transaction_result;
    return utils.send(StatusCodes.OK, {
      message: _.isEmpty(transaction_result) ? 'Transaction not found!' : 'Transaction retrieved successfully!',
      data: Item
    });
  } catch (e) {
    return utils.send(StatusCodes.INTERNAL_SERVER_ERROR, {
      message: e.message
    });
  }
};