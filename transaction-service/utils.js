'use strict';
const _ = require('underscore');
const AWS = require("aws-sdk");
const ses = new AWS.SES();
const sns = new AWS.SNS();

const { nanoid } = require('nanoid');

const send = (statusCode, data) => {
  const responseHeaders = {
    'Content-Type': 'application/json',
    // Required for CORS support to work
    'Access-Control-Allow-Origin': '*',  
    // Required for cookies, authorization headers with HTTPS
    'Access-Control-Allow-Credentials': true
  };
  return {
      statusCode: statusCode,
      headers: responseHeaders,
      body: JSON.stringify(
        data,
        null,
        2
      ),
    };
};

const getAppConfig = async () => {
  const appconfigdata = new AWS.AppConfigData();
  const session_params = {
    ApplicationIdentifier: 'NearApps', /* required */
    ConfigurationProfileIdentifier: 'TransactionsConfig', /* required */
    EnvironmentIdentifier: 'FeatureFlags', /* required */
    RequiredMinimumPollIntervalInSeconds: '300'
  };
  try {
    const tokenObj = await appconfigdata.startConfigurationSession(session_params).promise();
    const {InitialConfigurationToken: token} = tokenObj;
    const params = {
      ConfigurationToken: token /* required */
    };
    const configData = await appconfigdata.getLatestConfiguration(params).promise(); 
    const {Configuration} = configData;
    const stringConfig = Configuration.toString();
    return stringConfig ? JSON.parse(stringConfig) : stringConfig;
  } catch (e) {
    console.log('getAppConfig Error!! ', e.message);
    console.log('getAppConfig Error stack - ', e.stack);
  }
}

const constructUpdateExpressions = (inputParams) => {
  // Function to generate update expression for DynamoDB automatically based on the inputParams
  let updateExpression = '';
  const finalExpression = {};
  const expAttrNames = {};
  const expAttValues = {};
  const systemValReplacements = {
    status: 'x_status',
    type: 'x_type'
  };
  for (let attr in inputParams) {
    let emptyExpAttrNames = false;
    const replacedAttrVal = attr && systemValReplacements[attr] ? systemValReplacements[attr] : '';
    if (replacedAttrVal && !updateExpression) {
      updateExpression += `set #${replacedAttrVal} = :new_${attr}`;
    } else if (replacedAttrVal){
      updateExpression += `, #${replacedAttrVal} = :new_${attr}`;
    }
    if (!replacedAttrVal && attr && !updateExpression) {
      updateExpression += `set ${attr} = :new_${attr}`;
    } else if (!replacedAttrVal && attr){
      updateExpression += `, ${attr} = :new_${attr}`;
    }
    if (attr) {
      expAttValues[`:new_${attr}`] = inputParams[attr];
    }
    if (replacedAttrVal) {
      expAttrNames[`#${replacedAttrVal}`] = attr;
    }
  }
  if (!_.isEmpty(updateExpression)) {
    finalExpression.UpdateExpression = updateExpression;
  }
  if (!_.isEmpty(expAttrNames)) {
    finalExpression.ExpressionAttributeNames = expAttrNames;
  }
  if (!_.isEmpty(expAttValues)) {
    finalExpression.ExpressionAttributeValues = expAttValues;
  }
  return finalExpression;
};

const getNFT = (nft_id) => {
  console.log('getNFT >>> ', nft_id);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const tableParams = {
    TableName: "nfts",
    Key: {
      "nft_id": nft_id
    }
  };
  return docClient.get(tableParams).promise()
};

const getContacts  = (contact_ids) => {
  console.log('getContacts >>> ', contact_ids);
  if (!contact_ids || !contact_ids.length) {
    return;
  }
  const docClient = new AWS.DynamoDB.DocumentClient();
  const contactIDs = contact_ids.map(contact_id => {
      return {
        contact_id
      };
    });
  let queryParams = {
    RequestItems: {
      contacts: {
        Keys: contactIDs
      }
    }
  };
  return docClient.batchGet(queryParams).promise();
};

const getUser = (user_id) => {
  console.log('getUser >>> ', user_id);
   const docClient = new AWS.DynamoDB.DocumentClient();
  const tableParams = {
    TableName: "users",
    Key: {
      "user_id": user_id
    }
  };
  return docClient.get(tableParams).promise();
};

const getUsers = (user_ids) => {
  console.log('getUsers >>> ', user_ids);
  if (!user_ids || !user_ids.length) {
    return;
  }
  const docClient = new AWS.DynamoDB.DocumentClient();
  const userIDs = user_ids.map(user_id => {
      return {
        user_id
      };
    });
  let queryParams = {
    RequestItems: {
      users: {
        Keys: userIDs
      }
    }
  };
  return docClient.batchGet(queryParams).promise();
};

const createNFT = (params) => {
  console.log('createNFT >>> ', params);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const nft_id = params.nft_id ? params.nft_id : nanoid();
  const tableParams = {
    TableName: "nfts",
    Item: {
        "nft_id": nft_id,
        ...params,
        "status": "unclaimed_gift",
        "created": +new Date,
        "updated": +new Date
    }
  };
  return docClient.put(tableParams).promise();
};

const createTransaction = (params) => {
  console.log('createTransaction >>> ', params);
  const docClient = new AWS.DynamoDB.DocumentClient();
  const tableParams = {
    TableName: "transactions",
    Item: {
        "transaction_id": params.transaction_id,
        "sender_id": params.sender_id,
        "transaction_item_id": params.transaction_item_id,
        "transaction_value": params.transaction_value,
        "type": params.type,
        "created": +new Date,
        "updated": +new Date,
        "blockchain_status": "pending",
        "status": params.status
    }
  };
  if (params.recipient_id) {
    tableParams.Item.recipient_id = params.recipient_id;
  }
  return docClient.put(tableParams).promise();
};

const getNotificationObjectsForUsers = async (users, ntr_pairs) => {
  console.log('getNotificationObjectsForUsers >>> ', users, ntr_pairs);
  // check if the recipient exists in our system, if yes then don't send email
  let contactsToNotify = [];
  const emailNotifications = [];
  const smsNotifications = [];
  const usersToNotify = users && Array.isArray(users) ? users.slice() : [];
  const user_details = await getUsers(usersToNotify);
  if (user_details && user_details.Responses && user_details.Responses.users && user_details.Responses.users.length) {
    // Remove these users from the original bunch for notifications
    contactsToNotify = user_details.Responses.users.filter(user => usersToNotify.indexOf(user.user_id) === -1);
  } else {
    // copy all users from original bunch to contactsToNotify
    contactsToNotify = usersToNotify && usersToNotify.length ? usersToNotify.slice() : [];
  }
  if (!contactsToNotify.length) {
    console.log('No contacts to notify!', contactsToNotify);
  }
  const contact_details = await getContacts(contactsToNotify);
  // check here
  const { Responses } = contact_details ? contact_details : {};
  const { contacts } = Responses ? Responses : {};
  if (contacts && contacts.length) {
    // Build contact_id based map
    const contactNTRMap = {};
    ntr_pairs.map(ntr_pair => {
      const {nft_id, transaction_id} = ntr_pair;
      contactNTRMap[ntr_pair.recipient_id] = {
        nft_id,
        transaction_id
      };
    });
    contacts.map(contact => {
      const notifyContact = contact.contact_id && contactNTRMap[contact.contact_id] ? {
        ...contactNTRMap[contact.contact_id]
      } : {};
      if (contact.phone && contact.phone.length) {
        const mobilePhones = contact.phone.filter(phone => phone.type === 'mobile');
        const [phoneToNotify] = mobilePhones && mobilePhones.length ? mobilePhones : contact.phone;
        if (phoneToNotify && phoneToNotify.number) {
          smsNotifications.push({number: phoneToNotify.number, ...notifyContact});
        }
      } else if (contact.email && contact.email.length) {
        const personalEmails = contact.email.filter(email => email.type === 'personal');
        const [emailToNotify] = personalEmails && personalEmails.length ? personalEmails : contact.email;
        if (emailToNotify && emailToNotify.address) {
          emailNotifications.push({
            firstName: contact.first_name,
            lastName: contact.last_name,
            address: emailToNotify.address,
            ...notifyContact
          });
        }
      }
    });
  } else {
    console.log('No contacts to notify!', contacts);
  }
  return {
    emailNotifications,
    smsNotifications
  };
}

const sendSMS = async (phone, sender) =>{
  if (_.isEmpty(sender) || _.isEmpty(phone) || !phone.number) {
    console.log('sendSMS failed - either phone or sender is empty!', phone, sender);
    return;
  }
  if (!_.isEmpty(phone) && !phone.nft_id) {
    console.log('sendSMS failed - nft_id is empty!', phone.nft_id);
  }
  const params = {
    Message: `NearAPPs - ${sender.full_name} has sent you an NFT. Claim it by clicking this link - https://dev.nftmakerapp.io/nft/detail/claim/${phone.nft_id}`,
    PhoneNumber: phone.number
  };
  console.log('params >>> ', params, sender);
  return sns.publish(params).promise();
}

const sendEmail = async (emailObj, sender) => {
  const {firstName: recipientName, address: recipientEmail, nft_id} = emailObj;
  if (_.isEmpty(sender) || !recipientEmail) {
    console.log('sendEmail failed - Either email or sender is empty!', recipientName, recipientEmail, sender);
    return;
  }
  if (!nft_id) {
    console.log('sendEmail failed - nft_id is empty!', nft_id);
    return;
  }
  const sender_email = process.env.FROM_EMAIL;
  const params = {
      Destination: {ToAddresses: [recipientEmail]},
      Message: {
          Body: {
              Html: {
                  Charset: 'UTF-8',
                  Data: `
                    <html>
                      <body>
                        ${recipientName ? `<p>Hello ${recipientName},</p>` : ''}
                        <p>${sender.full_name} has sent you an NFT using the NFT Maker!</p>
                        <p>Please click <a href="https://dev.nftmakerapp.io/nft/detail/claim/${nft_id}">here</a> to claim it</p>
                        <br>
                        <p>If the above hyperlink doesn't work, please use the below link to claim</p>
                        <p>https://dev.nftmakerapp.io/nft/detail/claim/${nft_id}</p>
                      </body>
                    </html>`
              }
          },
          Subject: {
              Charset: 'UTF-8',
              Data: ${sender.full_name ? `NFT Maker - ${sender.full_name} has sent you an NFT!` : `NFT Maker - Someone has sent you an NFT!`}
          }
      },
      Source: sender_email
  };
  console.log('params => ',params);
  return ses.sendEmail(params).promise();
}

const triggerSMSNotificationsForUsers = async (phoneNumbers, sender) => {
  console.log('triggerSMSNotificationsForUsers >>> ', phoneNumbers);
  const phoneNumberRequests = phoneNumbers.map(phoneNumber => sendSMS(phoneNumber, sender));
  try {
    await Promise.all(phoneNumberRequests);
    console.log('SMS Notifications Sent!', phoneNumbers);
  } catch(e) {
    console.log('SMS Notifications Failure!', e.message);
  }
};

const triggerEmailNotificationsForUsers = async (emails, sender) => {
  console.log('triggerEmailNotificationsForUsers >>> ', emails);
  const emailRequests = emails.map(emailObj => sendEmail(emailObj, sender));
  try {
    await Promise.all(emailRequests);
    console.log('Email Notifications Sent!', emails);
  } catch(e) {
    console.log('Email Notifications Failure!', e.message);
  }
};

const triggerNotificationsForUsers = async (params, users) => {
  console.log('triggerNotificationsForUsers >>> ', users);
  const {sender_id, ntr_pairs} = params;
  const sender_details = await getUser(sender_id);
  let senderObj = sender_details && sender_details.Item ? sender_details.Item : {};
  senderObj = {
    ...senderObj
    // nft_id: params.transaction_item_id
  };
  const {emailNotifications, smsNotifications} = await getNotificationObjectsForUsers(users, ntr_pairs);
  if (emailNotifications && emailNotifications.length) {
    await triggerEmailNotificationsForUsers(emailNotifications, senderObj);
  }
  if (smsNotifications && smsNotifications.length) {
    await triggerSMSNotificationsForUsers(smsNotifications, senderObj);
  }
};

module.exports = {
  send,
  constructUpdateExpressions,
  createNFT,
  createTransaction,
  getNFT,
  triggerNotificationsForUsers,
  getAppConfig,
  getContacts,
  getUsers
};