const AWS = require("aws-sdk");
const utils = require("./utils");
const moment = require('moment');

AWS.config.update({
  region: process.env.REGION
});

module.exports.main = async (event) => {
  // tid, from, to, nft_id, created, updated, status
  const {pathParameters} = event;
  const {user_id} = pathParameters;
  const docClient = new AWS.DynamoDB.DocumentClient();

  try {
    const tableParams = {
      TableName: "transactions",
      // ProjectionExpression: "relationid",
      FilterExpression: "sender_id = :fromID OR recipient_id = :toID",
      ExpressionAttributeValues: {
        ":fromID": user_id,
        ":toID": user_id
      }
    };
    // console.log(JSON.stringify(tableParams));
    // console.log('user_id - ', user_id);
    const transaction_results = await docClient.scan(tableParams).promise();
    console.log(JSON.stringify(transaction_results));
    const transactions = transaction_results && transaction_results.Items ? transaction_results.Items : [];
    if (transactions && transactions.length) {
      const counterparty_ids = [];
      transactions.map(transaction => {
        if (transaction.sender_id && transaction.sender_id === user_id) {
          transaction.sender = true;
          if (transaction.recipient_id && counterparty_ids.indexOf(transaction.recipient_id) === -1) {
            counterparty_ids.push(transaction.recipient_id);
          }
        } else {
          if (transaction.sender_id && counterparty_ids.indexOf(transaction.sender_id) === -1) {
            counterparty_ids.push(transaction.sender_id);
          }
        }
        transaction.formattedtime = moment(transaction.updated).fromNow();
      });
      // console.log('counterparty_ids ---> ', counterparty_ids);
      if(counterparty_ids && counterparty_ids.length) {
        const userResultsFromDB = await utils.getUsers(counterparty_ids);
        // console.log('userResultsFromDB ---> ', userResultsFromDB);
        let user_results = (userResultsFromDB && userResultsFromDB.Responses && userResultsFromDB.Responses.users) ? userResultsFromDB.Responses.users : [];
        if (!user_results || !user_results.length) {
          // If user doesn't exist in users db, we are checking contact db to source details
          const contactResultsFromDB = await utils.getContacts(counterparty_ids);
          // console.log('contactResultsFromDB ---> ', contactResultsFromDB);
          user_results = (contactResultsFromDB && contactResultsFromDB.Responses && contactResultsFromDB.Responses.contacts) ? contactResultsFromDB.Responses.contacts : [];
        }
        if (user_results && user_results.length) {
          // Create counterparty object
          const cp_map = {};
          user_results.map(party => {
            // console.log('party --> ', party);
            const {wallet_id, first_name, last_name} = party;
            const user_result_id = party.user_id || party.contact_id;
            let {full_name, email, phone} = party;
            if (!full_name && !wallet_id) {
              // Case to identify that the record is from contacts table
              full_name = `${first_name} ${last_name}`;
            }
            if (phone && Array.isArray(phone) && !wallet_id) {
              // Case to identify that the record is from contacts table
              // console.log('phone - ', phone);
              full_name = `${first_name} ${last_name}`;
              const mobilePhones = phone.filter(ph => ph.type === 'mobile');
              [phone] = mobilePhones && mobilePhones.length ? mobilePhones : phone;
              phone = phone && phone.number ? phone.number : phone;
            }
            if (email && Array.isArray(email) && !wallet_id) {
              // Case to identify that the record is from contacts table
              full_name = `${first_name} ${last_name}`;
              const personalEmails = email.filter(em => em.type === 'personal');
              [email] = personalEmails && personalEmails.length ? personalEmails : email;
              email = email && email.address ? email.address : email;
            }
            cp_map[user_result_id] = {wallet_id, full_name, email, phone};
          });
          // console.log('cp_map ---> ', cp_map);
          transactions.map(transaction => {
            console.log(transaction.sender_id, transaction.recipient_id, user_id);
            if (transaction.sender_id && transaction.sender_id === user_id && transaction.recipient_id) {
              transaction.counterparty = cp_map[transaction.recipient_id];
            } else if (transaction.sender_id) {
              transaction.counterparty = cp_map[transaction.sender_id];
            }
          });
        }
      }
    }
    return utils.send(200, {
      message: 'Transactions retrieved successfully!',
      data: transactions
    });
  } catch (e) {
    return utils.send(500, {
      message: e.message
    });
  }
};